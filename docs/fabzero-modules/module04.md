# 4. Outil Fab Lab sélectionné

## Introduction
Lors de ce module, nous avons reçu une formation de notre choix entre la CNC, la découpeuse laser, le microprocesseur, l'impression en 3D et la réparation grâce à l’imprimante 3D. J'ai personnellement choisi la formation sur les microprocesseurs et la réparation avec l’imprimante 3D.
J’ai d’abord débuté par celle sur le microprocesseur qui m’a permis de comprendre son potentiel et surtout certaines de ses applications.

Pour ma part, en tant que biologiste, la partie « sensor » m'a fortement intéressé, car elle est applicable au niveau des laboratoires pour récolter par exemple des données de température, humidité, pendant les cultures de bactéries ou de champignons.
## D’abord qu’est-ce qu’un microprocesseur ?
Un microprocesseur est une plaquette électrique qui peut recevoir du code depuis un ordinateur, le lire et faire ce que le code lui dit de faire, par exemple faire clignoter une ampoule en rouge toutes les deux secondes.

Ce qui est intéressant avec le microprocesseur, c'est que des « pièces » différentes peuvent être ajoutées, par exemple des ampoules et des boutons. Ce qui le rend d’autant plus intéressant, c'est qu’il ne coûte pas si cher, on peut compter autour de 3/5 euros pour le microprocesseur seul et environ une trentaine d’euros pour un kit.

## Les logiciels
Pour utiliser le microprocesseur, nous avons vu trois différentes approches. La première qui est la plus facile pour une première approche, c'est Arduino qui nous permet d’encoder des lignes de code dans le microprocesseur pour qu’il fasse différente action. Cependant, Arduino est assez simpliste et est rapidement saturé pour des tâches complexes. La deuxième méthode est d’utiliser python à travers des logiciels comme Thonny qui permet d’aller plus loin. La troisième méthode est celle utilisée dans le domaine professionnel et celle avec C++ qui permet des actions complexes comme avec les « sensors » par exemple.

### Arduino
Nous avons dû dans la première partie utiliser Arduino, en se basant sur un exemple de code qui permet de faire clignoter une ampoule et de le modifier pour que la couleur qui clignote change. J’ai personnellement eu beaucoup de mal au début, le temps de comprendre comment fonctionne Arduino.

Nous n’avions eu que très peu de liens et d'informations pour comprendre comment Arduino fonctionne et comment le microprocesseur fonctionne avec Arduino donc j’étais un peu perdu. Après la première séance, j'ai décidé de bouger en réparabilité pour le reste des formations.
## La réparabilité
Cette formation a été super intéressante, car je ne m’y connaissais pas du tout en électricité. Pendant la première séance, on nous a d’abord expliqué les différentes étapes de la réparation pour être plus efficace et comprendre le plus rapidement possible d’où vient le problème.

Nous avons eu comme cas pratique une boîte qu'on a dû ouvrir, elle contenait un circuit électrique brancher à une ampoule, une batterie et un interrupteur qui était placé dans la boîte en bois vissé, la lampe ne s'allumer pas. Pour accéder au système électrique, on a dû enlever tous les compartiments qui nous bloquaient. Puis, on a testé avec un multimètre les différents fils du circuit avec la fonction testeur de continuité, si la batterie était chargée avec la fonction courant continu qui devait afficher autour de 5V, l’interrupteur et l’ampoule à leurs bornes jusqu’à ce qu’on trouve le problème et qu’on remplace la pièce défectueuse. Dans mon circuit, c'était **un file** qui était défectueux.

En réparabilité, il faut être très méthodique lorsque l’ont test le système pour ne rien oublier. Le plus important, c'est d'être rigoureux lorsqu'on prend l'objet en main pour trouver le problème, d'observer s'il y a des cassures avant même de l'ouvrir avec un tournevis ou une scie, si l'objet scellé.

Voici des photos ci-dessous à quoi ressemblait la boîte en bois qu'on a dû démonter.

![](images/boite1.jpg)

![](images/boite2.jpg)

De plus, on a aussi eu un aperçu sur tous les autres composant qu'un système électrique peut avoir dans des appareils électroménagers(exemple : résistance, une diode, etc) qui peut potentiellement être la source du problème lorsqu'un appareil ne fonctionne pas.

![](images/tout.jpg)

Pendant la deuxième séance, on a dû ramener un objet cassé et se mettre binôme. Le but était de dessiner une pièce en 3D sur un logiciel, on a personnellement utilisé OpenScad (open source). On a choisi comme objet une lumière de vélo de chez Décathlon ou le morceau de plastique qui permettait de le fixer au vélo s'était cassé.

Mon binôme a commencé par scié l'embout qui été cassé pour avoir une pièce plane, qu'il a ensuite poncée au papier de verre pour améliorer la finition.

![](images/scie.jpg)

Puis, on a créé une pièce qui fonctionne un peu comme une coque de téléphone. La photo en dessous montre le premier prototype de la pièce qui n’est pas encore fini.

Voici le socle de base :

```
larg =26.5;
long = 45;
haut = 13;
side = 26.6;

module socle(){
difference(){
//piece autour
translate([0,0,0])
cube([(long + 0.2)+3,(larg +0.2)+3,(haut +0.3)+3],center =true);


//cube qui creuse
translate([0,0,1.6])
cube([long -0.2,larg -0.2,haut +0.3],center= true);
}
}

socle();

```

![](images/proto_1.jpg)

On a ensuite rajouté une accroche qui permet d’accrocher la lampe au vélo ou à son sac par exemple quand on roule.

![](images/proto_2.jpg)

![](images/proto_3.jpg)

On a ensuite réfléchi au fait que si on mettait notre lampe dans notre “cover”, pour éviter de devoir l’enlever sans arrêt et du coup fragiliser la pièce, nous avons fait une ouverture à l'avant du “cover” pour donner accès au chargeur.

![](images/b_twin.jpg)

![](images/ouverture.jpg)

Voici le code final de notre prototype :

```
//hauteur de la lampe= 1.28 cm
// largeur de la lampe = 2.65cm
//longeur de la lampe = 4.5cm
larg =26.5;
long = 45;
haut = 13;
side = 26.6;


module socle(){
difference(){
//piece autour
translate([0,0,0])
cube([(long + 0.2)+3,(larg +0.2)+3,(haut +0.3)+3],center =true);


//cube qui creuse
translate([0,0,1.6])
cube([long -0.2,larg -0.2,haut +0.3],center= true);
}


//pièce pencher pour former l'attache
rotate([0,-side,0])
translate([(-0.65)*(larg),0,-((0.000005)*haut)])
cube([haut*0.26,(2/3)*larg,haut*1.4],center =true);

// piece qui est sur le cube de contour et qui permet de bloquer

translate([haut*0.7,0,1])
rotate([0,side*6,0])
cube([haut*1.5,(0.1)*larg,haut*1.7],center =true);
}

module piece_troue(){
difference(){
//pièce  relier a la piece incliné
translate([(0.12)*(larg),0,-haut*1.5])
cube([long *0.7,(2/3)*larg,0.2*haut],center =true);

//piece qui troue dans la piece incliné
translate([(0.2)*(larg),0,-haut*1.5])
cube([long*0.45,larg*0.34,(0.4*haut)+0.4],center =true);
}
}

module piece_total(){
union(){   
translate([0,0,-4.3])
socle();
piece_troue();

}
}

module piece_av(){
difference(){
piece_total();

//bloque qui creuse les imperfections
translate([0,0,0.6])
cube([long + 0.2,larg +0.2,haut +7],center= true);

}
}

module fin(){
difference(){   
piece_av();

translate([23.5,0,-4])
cube([1.9,(larg +0.2)+4,(haut +0.3)+5],center =true);
}
}
fin();

```
On a finalement imprimé notre "case" pour notre lumière de vélo qui a fonctionné dès la première impression avec quand même quelques problèmes de finition, mais l'outil en lui-même fonctionne !!

![](images/velo.jpg)

![](images/velo2.jpg)

![](images/velo3.jpg)













