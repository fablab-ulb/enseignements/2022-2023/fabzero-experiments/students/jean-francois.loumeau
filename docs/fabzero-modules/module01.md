# 1. Introduction à la documentation

Lors de notre première séance, nous avons eu un moment pour nous rencontrer tous et discuter de l'horizon dont on venait. Cet échange a été suivi par une petite visite du Fab Lab en passant à travers différents laboratoires d'expérimentation, où on a eu droit à des démonstrations de projets en cours.

Pour la deuxième séance, le but était de nous familiariser avec la documentation qui est un pilier du cours et qui nous suivra jusqu'au projet final. Une introduction aux sciences frugale aussi a eu lieu, en mettant en avant son importance et son application dans les fabrications digital.

Malheureusement, je n'ai pas pu être présent à cette séance, n'y a la séance de mise en pratique, mais j'ai tout de même pu comprendre les enjeux de la documentation en rattrapant l'enregistrement.
Cet assignment est une mise en pratique de ces principes à travers un logiciel de traitement de texte, qui renvoie ensuite notre travail sur notre GitLab à partir de commandes depuis un terminal Linux.

# Linux command line

Nous avons toujours été habitués à travailler sur des programmes à travers des **Graphical User Interface GUI**, mais il est important de savoir ce qu'il se passe sous le capot pour avoir un meilleur contrôle de son ordinateur. Nous avons été introduits dans ce module au **Command Line user Interface CLI**, plus communément appelé **terminal**, à travers le terminal d'**UNIX**.

Le terminal qu'on a utilisé s'appelle **Bash(UNIX shell)**. Les avantages d'utiliser ce terminal est de pouvoir utiliser les programmes qui n'ont pas d'interface graphique, il est plus rapide et moins lourd. Le principe de **linux** est d'utiliser de petits programmes très performant individuellement, qui ne peuvent exécuter qu'une commande, mais il est possible de lier ces petits programmes entre eux pour créer quelque chose de plus poussé.

Nous avons découvert les différentes commandes de base pour pouvoir utiliser Bash, comme **cd**,**ls**,**pwd**, etc.

| Commands    | Description                      | Description              |
|-------------|----------------------------------|--------------------------|
| ls          | List files and folders           | Display hidden files     |    
| pwd         | Print working directory          |                          |
| mkdir       | Create a directory               | Parent directories       |
| cd          | Change directory                 | Go to the last directory |
| cp          | Copy files and folders           |                          |
| mv          | Move files and folders           | Recursive                |
| rm          | Remove files and folders         |                          |
| find        | Find files and folders           |                          |

## La documentation

### Pourquoi perdre son temps à documenter ?

Malgré le sentiment parfois de perdre son temps en passant des heures à résumer ce qu'on a fait, la documentation est une méthode de travail qui est cruciale. Elle est importante pour deux raisons principales.
Avant tout pour nous même, elle nous permet de garder une trace au propre de notre apprentissage pour pouvoir revenir sur nos notes lors d'un prochain projet et de pouvoir quantifier notre avancement, de mieux nous organiser.
Elle a un deuxième avantage majeur qui fait un peu partie des valeurs du Fab lab, c'est le partage de connaissance. En rédigeant de manière pragmatique et claire nos expérimentations, cela peut permettre de créer un support de travail pour d'autres personnes qui ont besoin de passer par le même chemin que nous et peut être inspiré certain.

Pour documenter, nous avons téléchargé un logiciel de traitement de texte des plus basiques pour décrire notre avancement, j'utilise personnellement **Visual studio code** qui fait le boulot.

Dans ce logiciel, on a dû apprendre à écrire en **Markdown**, c'est ça qui va nous permettre de mettre des titres, des mots en gras, faire des listes, etc, quand ça sera sur notre site. On peut aussi y ajouter des images et vidéos qui doivent être retravaillé à travers **Graphics Magick**, par exemple pour les images, cela va permettre d'atténuer la résolution pour avoir une image moins lourde et donc prendre moins de place dans les serveurs.

### Version control systeme

"un système de contrôle de version (ou VCS, pour Version Control System) est un utilitaire logiciel qui suit et gère les changements apportés à un système de fichiers."

Il nous permet de conserver les enregistrements de notre travail et au fil de notre avancement. Il est pratique dans la mesure où si on a enregistré notre travail, mais que la version antérieure nous convenait plus, on peut revenir à celle-ci. Pour cela, nous avons utilisé Git lab qui convenait parfaitement à nos besoins et à la philosophie du fab zéro.

## Comment utiliser Git ?

J'ai découvert Git à travers ce module qui est un outil très intéressant en matière de partage de connaissance. Son installation a été pour moi au début assez compliqué en tant que novice de l'informatique, mais j'ai finalement pu m'en sortir.

## Automatic deployment et git

En utilisant une **"working copy"** sur son ordi, on peut faire des modifications sur la version locale avant de l'envoyer sur notre site. C'est aussi beaucoup plus rapide que la version **GUI**, et ça permet aussi de travailler sur projet en collaboration avec plusieurs personnes.

Pour ça, on a dû télécharger **git**, qui est un outil très intéressant en matière de partage de connaissance. Son installation a été pour moi au début assez compliqué en tant que novice de l'informatique, mais j'ai finalement pu m'en sortir. J'ai dû d'abord le [télécharger](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git), le [configurer sur mon ordi](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git)(user.name;user.email)et mettre en place une clé [**SSH**](https://docs.gitlab.com/ee/user/ssh.html) qui m'a permis cette connexion entre la copy local et le site qui est sécurisé et enfin de cloner une copie de mon travail sur mon ordi via cette clé SSH, le tout grâce au tutoriel de nos professeurs. L'activation de la clé s'est faite à travers notre terminal auquel nous avons "coller" le lien de la SSH key qu'on a eu sûr Gitlab dans les user settings. J'ai personnellement utilisé la clé **ED25519**.
J'ai beaucoup été aidé par Denis et Nicolas pour cette partie, car ça ne fonctionnait pas avec mon ordinateur, mais on a tout de même pu régler le problème.

En utilisant des commandes très simples (ci-dessous), on peut envoyer notre travail sûr GitLab en ligne qui va mettre à jour notre site deux fois par semaine.

![](images/1_commandgit.jpg)

## Principe de la gestion de projet

Nous avons été initiés à plusieurs méthodes de travail pour l'organisation au niveau du temps, mais aussi au niveau des bonnes habitudes à prendre dès le début.

Voici les différentes techniques dont nous avons parlé :

* **Modular & Hierarchical Planning** : Consiste à diviser le problème en plusieurs morceaux plus facile à résoudre et ensuite de rassembler le tout.

* **As-You-Work Documentation** : elle consiste à documenter après chaque session de travail sur le projet ce que nous avons appris, découvert et les nouvelles compétences aquises. Cela permet de garder des traces de l'apprentissage pour potentiellement la partager et pour nous de pouvoir garder en mémoire notre avancée.

* **Spiral development** : La méthode la plus pragmatique pour pouvoir garder la cadence et rester dans les temps est la méthode de la **Spiral** . Elle nous dit qu'à chaque session de travail le plus important est de se fixer un nombre d'heures par tâche et de poser un premier bloc même s'il est un peu brouillon et pas fini, cela permet de mieux évaluer le travail qui reste à faire et à être plus performant.

![](images/1_spiral.jpg)

* **Demand vs Supply - side Time Management** : C'est une technique basée sur la gestion du temps qui consiste à évaluer le temps de travail pour notre projet et ensuite de diviser notre travail en sous tâche qui ont chacune un temps donné pour être effectué.

![](images/1_time.jpg)

**Hofstadter’s Law**:

“It always takes longer than expected, even taking into account Hofstadter’s Law.”

