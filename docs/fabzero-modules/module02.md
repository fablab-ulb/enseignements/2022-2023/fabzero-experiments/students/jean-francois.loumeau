# 2. Conception Assistée par Ordinateur (CAO)

Cette semaine, nous avons fait la conception sur Openscad d'un objet en 3D. Le but était de chacun crée une pièce qui représentait une partie d'un objet, le tout sera imprimé et ensuite assemblé. L'objet devait posséder un "compliant mechanism", on a opté pour un jump stick comme objet final.

![](images/2_jumpstick_R.jpg)

Il existe plusieurs logiciels qui permettent de faire des de la CAO comme "Inkscape" qui sera plutôt utilisé pour de la conception en 2D(exemple : logo), il est libre d'accès. Il existe aussi des logiciels comme Fusion 360 qui sont aussi très performants, mais nécessite une licence, ce qui est un risque pour l'utilisateur en cas de faillite ou augmentation des prix.

Ce qui est intéressant avec Openscad c'est que tout se fait à partir de lignes de code, grâce à des fonctions qu'on retrouve dans le "cheat sheet" dans l'onglet help.
## Prise en main du logiciel

J'ai d'abord commencé par me familiariser avec le logiciel en dessinant des formes simples pour comprendre comment fonctionnent les différentes fonctions et les formes basiques comme le cube ou le cylindre.

cube([largeur, profondeur, hauteur], center)       


```
cube([4,6,8],center= true);
```
Exemple:

![](images/2_cube_simple_R.jpg)

cylinder(hauteur, rayon_inférieur, rayon_supérieur)
```
side =5;
cylinder(side*10,side,side,center=true);          
```

Exemple:

![](images/2_cylindre_simple_R.jpg)       

Ensuite je me suis plutôt intéressé à la fonction translate et rotate qui permet pour la première de déplacer la figure sur les axes(x,y,z) et la deuxième, de les réorientés par rapport aux axes.

Exemple translate  :
```
side =5;
translate([90,0,0])
cylinder(side*10,side,side,center=true);
```
![](images/2_cylindre_translate_R.jpg)  

Exemple rotate :
```
side =5;
$fn= 25;
rotate([90,0,0])
cylinder(side*10,side,side,center=true);                                   
```
![](images/2_photo_rotate_R.jpg) 


Il y a aussi la fonction $fn qui permet de donner une meilleure résolution aux formes, mais cela demande beaucoup plus de jus au pc, on travaille en général entre 0-50 pendant la réalisation de la pièce et à la fin, on la passe à 100-150.

Exemple avec $fn = 25 et $fn = 100 :

![](images/2_fn1.jpg)      ![](images/2_fn100_R.jpg)

## Corps du jump stick

Pour cet assignment, j'ai travaillé avec Jules Gérard. Nous avons démarré par la pièce qui compose le corps de notre objet ensemble et il a ensuite poursuivi son travail dessus pendant que moi, je me suis concentré sur la deuxième pièce qui est le "compliant mechanism", j'aborderai quand même un peu la conception de la première pièce qui nous a permis de comprendre le programme Openscad.

# Barre principale

On a tout d'abord débuté par la barre principale sur laquelle tous les autres compartiments vont venir s'ajouter par la suite. C'est tout simplement un parallélépipède rectangle.

![](images/barre_principale.jpg)

Pour la partie écrite, on a décidé de mettre le code pour la barre principale et le cylindre qui représente l'endroit où l'utilisateur met ses mains dans le même module.
```
module barre_v(){ // barre verticale
 difference(){
cube([side/sqrt(2),side/sqrt(2),side*20],center=true);
  rotate([90,0,0])
translate([0,side*9.5,0])
cylinder(side*6,side/3.9,side/3.9,center = true);
}
// cylindre poignées
rotate([90,0,0])
translate([0,side*9.5,0])
cylinder(side*6,side/4,side/4,center = true);
}
```

On a ensuite ajouté deux barres horizontales. La première en partant du haut est une barre qui va coulisser le long de la barre principale et sera reliée à un côté du compliant mecanism. La deuxième barre qui est plus courte est placée plus bas sur le jumping stick, elle sera fixée à la barre principale et reliée à la deuxième extrémité du compliant mechanism.
C'est ce système de fixation du compliant mechanism aux deux barres qui est censées avec le mouvement de l'utilisateur avoir un effet de ressort.

Voici une photo de la barre horizontale coulissante :

![](images/barre_coulissante.jpg)

Voici une photo de la barre horizontal fixe en rose :

![](images/barre_fixe.jpg)

Nous avons réfléchi ensemble sur la conception des barres, mais c'est lui qui s'est occupé de creuser les trous dans les barres pour fixer la deuxième pièce.

## Compliant mechanism

La conception du compliant mechanism a été la partie la plus compliquée pour moi, car il a fallu utiliser les nouvelles "fonctions" qui sont la création de modules, l'union entre deux ou des structures et le hull qui permet de combiner des structures pour ne former au final qu'une structure.
La deuxième épreuve majeure a été de creuser les trous dans la pièce à chaque extrémité pour qu'elle puisse se fixer grâce à la fonction difference().
Nous avons essayé au maximum de tout paramétrer, c'est-à-dire de remplacer les valeurs par des variables placer en tête du code qu'on peut modifier à tout moment.

Voici la deuxième pièce :

![](images/smallimage_R.jpg)  

Et son code :
```
// File : piece_compliant_final_troué.scad

// Author : Martin Loumeau

// Date : 01/03/2023

// License : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)

side = 5;
  height = 10; //hauteur cylindre qui forme la pièce
  c_bas = 5;//rayon cylindre qui forme la pièce
  c_haut = 5;//rayon cylindre qui forme la pièce
  distance =c_bas;
  dist = 0.6;
  c_bas2 = 3;//rayon cylindre qui creuse
  c_haut2 = 3;//rayon cylindre qui creuse
  ecart = dist+9;
  height2 = 20;//hauteur des cylindre qui creuse
  lg = 50; //longeur tige
  ht = 4;
  largt = 2; // largeur de la tige
  distance2 = 0; //distance entre les trous
  corec = 0.5; //correction pour les pièce ce touchent
 //double cylindre qui forme la structure de fixation
module doublecylindre()
{
  side = 5;
  height = 10;
  c_bas = 5;
  c_haut = 5;
  distance =10;
   union(){
   
      cylinder(height,c_bas,c_haut,center= true);
      translate([0,distance,0])
      cylinder(height,c_bas,c_haut,center= true);
  }
}




//forme une seule pièce avec les deux cylindre
module cylindre_double_colle()
{
  hull(){
      doublecylindre(height,c_bas,c_haut);
  }
}


//Crée deux cylindre basé sur les dimensions des deux premiers pour creuser
module creuse()
{
  side = 5;
  height = 5;
  c_bas = 5;
  c_haut = 5;
  distance =10;
   union(){
      translate([0,distance2,0])
      cylinder(height2,c_bas2,c_haut2,center= true);
      translate([0,distance-distance2,0])
      cylinder(height2,c_bas2,c_haut2,center= true);
  }
}


//Permet de creuser la partie double
module piece()
{
  difference(){
    
    
      cylindre_double_colle(height,c_bas,c_haut);
      creuse(height2,c_bas2,c_haut2);  
  }
}


//forme la tige central
rotate([90,0,0])
translate([0,0,(-0.5*lg)-(3*c_bas)+0.2])
cube([largt,ht,lg],center =true);


//création de la partie simple avec un creux
difference()
  {
translate([0,(lg)+(4*c_bas)-corec,0])
cylinder(height,c_bas,c_haut,center= true);

translate([0,(lg)+(4*c_bas)-corec,0])
cylinder(height2,c_bas2,c_haut2,center= true);
  }


piece();
```

Par la suite nous avons remplacé les trous par des embouts pour éviter de devoir créer une autre pièce pour fixer le tout le compliant mechanism et le jump stick.

Voici le compliant mechanism final :

![](images/2_4_final_compliant.jpg)  

Et son code :
```
// File : piece_compliant_final.scad

// Author : Martin Loumeau

// Date : 04/03/2023

// License : Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) (https://creativecommons.org/licenses/by-nc-sa/4.0/)
$fn = 100;
  side = 5;
   long = side*20;//longueur de la tige
   distance2 = 0; //distance entre les embouts
   corec = 0.5; //correction pour les pièce se touchent
 //double cylindre qui forme la structure de fixation
module doublecylindre()
{
 
  union(){
   
      cylinder(side*2,side +2 ,side +2,center= true);
      translate([0,(side +2)*2,0])
      cylinder(side*2,side +2,side +2,center= true);
  }
}


//forme une seule piece avec les deux cylindre
module cylindre_double_colle()
{
  hull(){
      doublecylindre(side*2,side +2,side +2);
  }
}

//Crée deux cylindre basé sur les dimensions des deux premiers(mais plus petit) pour faire des embouts

module embout()
{
   union(){
      translate([0,distance2,side*1.5])
      cylinder(side,side,side,center= true);
    
      translate([0,((side +2)*2)-distance2,side*1.5])
      cylinder(side,side,side,center= true);
       cylindre_double_colle();
    
    
      }
}

//forme la tige central

module tige(){
rotate([90,0,0])
translate([0,0,(-0.5*long)-(3*side)-0.2])
cube([(side*(2/5)),side,(long)],center =true);
}
//création de la partie simple avec un creux

module piece_simple(){
//Pièce cylindre simple
translate([0,(long)+(4*side)-corec,0])
cylinder((side*2),side +2,side +2,center= true);

//embout simple
translate([0,(long)+(4*side)-corec,side*1.5])
cylinder((side),side,side,center= true);
 }

module piece(){

union()
  {
      embout();
      cylindre_double_colle();
      piece_simple();
      tige();
}
}

piece();
```
## Licence creative commons

En faisant de la création de musique, de pièces d'art ou même de design en 3D, nous avons d'office un droit d'auteur dessus. Pour certains projets, il est tout de même intéressant de rendre son travail libre d'accès pour que les autres l'utilisent ou s'en inspirent.

Les licences "creative commons" nous permettent de rendre notre travail accessible aux autres. Il existe différents types de licences avec des accès et des droits d'utilisation différents. Pour ma pièce, j'ai décidé d'utiliser la licence **CC BY-NC-SA** qui permet à tout le monde de modifier, remixé, adapté le code seulement s’il cite l'auteur du code, ils ne pourront par contre-pas commercialiser le projet.

![](images/2_5_licence.jpg)


