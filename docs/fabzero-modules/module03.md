# 3. Impression 3D

## 3.1. Introduction

Pour le troisième module, nous avons appris à nous servir d'une imprimante 3D pour imprimer nos pièces du deuxième module. Nous avons eu une présentation de l'imprimante 3D par Axel Cornu, qui nous a expliqué les différentes astuces pour que l'impression soit plus rapide et comment éviter les erreurs classiques.

L'impression 3D peut servir à fabriquer des pièces prototype et d'avoir un rendu assez rapide de l'objet conçu avant son industrialisation ou sa fabrication dans des plus grandes dimensions. Son avantage est qu'elle est de moins en moins coûteuse tout comme le matériel utilisé, et permet les limites de certaine structure.

### 3.2. Imprimante et logiciel

Le matériel utilisé sont des bobines de thermoplastique qui peuvent être de plusieurs sortes, PLA, ABS, etc.
Le Fab Lab utilise comme imprimante 3D,la FDM Prusa, qui peuvent être elle-même imprimé grâce à des fichiers open source. Il utilise aussi majoritairement comme thermoplastique le PLA car il dégage moins de fumée toxique que des matériaux comme l'**ABS**, l'**ASA** ou le **PS**.

Pour utiliser l'imprimante, il faut d'abord télécharger **Prusa Slicer** sur le [site officiel](https://help.prusa3d.com/article/install-prusaslicer_1903). Il faudra configurer le programme sur expert pour pouvoir faire des modifications ensuite.

Pour pouvoir utiliser son code qui a été écrit sur **OpenScad**, il faut :

- L'exporter en format **.stl** ou **.obj**.

![](images/export.jpg)

- l'importer sur notre logiciel d'impression qui est pour nous Prusa Slicer.

- Sélectionner l'imprimante qu'on veut utiliser

- Choisir un filament

## 3.3. Conseil pratique et erreurs a évité
Conseil sur Prusa Slicer:

* Toujours allongé la pièce sur sa plus grande surface dans Prusa Slicer.
* Ajout d'un support à la pièce dans certain cas pour qu'elle tienne.
* Conserver le taux de remplissage entre 15-35%, au-delà de ça, ça prendra beaucoup de temps pour la même solidité, 15% est un bon compromis.
* En dessous de 10% de remplissage, le support que donne le remplissage ne sera pas optimal.
* Utiliser la jupe comme test pour voir si le plateau est assez propre et s’il y a une bonne adhérence.
* Pour les pièces avec une petite surface en contact avec le plateau, une bordure est conseillée pour une meilleure adhérence.
* Optez pour une épaisseur de parois de "trois périmètres" pour que la pièce soit suffisamment solide.
* Le bouton en bas à droite **Découper maintenant** permet de voir le temps d'impression de notre pièce.
* Pour diminuer le temps, modifier la hauteur de couche ou le remplissage.
* Un curseur en bas à gauche permet de voir la pièce couche par couche pour voir si le niveau de détail est correct.

Conseil sur l'imprimante 3D :

* Toujours nettoyer sûr et sous son plateau avant l'impression avec du solvant
* Vérifiez qu'il n'a pas de nœud dans la bobine et que le filament est bien inséré dans l'imprimante pour éviter une impression dans le vide.

## 3.4. Impression de ma pièce et celle de mon binôme de soutien

### 3.4.1. Création d'une pièce de mesure

Nous avons dû créer une pièce de mesure pour que les dimensions du compliant mechanism soit compatible avec les points de fixation sur le jump stick, pour imprimer toute notre structure qu'une fois et éviter la perte de temps et le gaspillage. Pour cela, nous créerons une petite pièce avec des embouts avec différentes dimensions.

On a mis un embout qui a le même rayon que celui du trou des pièces de fixation sur le jump stick et des embouts qui augmente de rayon par 0.1 à droite et qui réduit leurs rayons de 0.1 à gauche.

### Outils de mesure :

![](images/2_piece_mesure.jpg)

### 3.4.2.Compliant mechanism

#### Première essaie 

![](images/c1.jpg)

Le première essaie a été un échec parce que j'avais oublié de mettre des supports et la tige reliant les deux extrémités ne touchait pas le plateau.

#### Rendu du compliant mechanism

![](images/c2.jpg)

![](images/c3.jpg)

### 3.4.3. Jump Stick

On a imprimé uniquement notre bar central et un outil de mesure pour le rayon du trou de la bar ou on place ses pieds. Malheureusement notre bar était assez fragile et les finitions n'était pas terrible, mais c'était quand même intéressant de pouvoir imprimer une première fois.

Bar central :

![](images/i_bar.jpg)


![](images/i_zaf.png)


Outils de mesure :


![](images/i_mesure.jpg)







