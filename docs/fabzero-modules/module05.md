# 5. Dynamique de groupe et projet final

## Introduction

Cette semaine, nous avons dû apporter un objet qui représente une thématique. Nous avons expliqué pourquoi nous avions choisi cet objet et formé des groupes avec des personnes dont l'objet pouvait être mis en relation, par exemple une algue et un coquillage.

## 5.1. Création du problème en arbre

Pendant ce cours, nous avons eu une présentation sur "l'arbre à problème" et "l'arbre à solution" qui avait pour but de mieux cerner la problématique en ayant tout sous les yeux.

Cette méthode consiste à dessiner un premier arbre avec des branches et des racines, et au centre du tronc le problème principal. Les racines représentent les causes du problème et les branches représentent ce que les racines et le tronc ont comme conséquence.

Le deuxième arbre sera orienté vers les solutions au problème, avec sur le tronc le résultat qu'on voudrait atteindre et les racines représenteront les différentes voies pour atteindre cet objectif. Les branches quant à elles correspondent aux impacts du changement sur la société ou l'environnement. Le but est d'aller en profondeur en essayant d'amener la réflexion plus loin avec des branches secondaires.

C'est une méthode intéressante pour notre projet de groupe, elle nous permettra de trouver une problématique plus rapidement et de meilleure qualité.

### 5.1.1 Exemple d'arbre à problème


Pour mon arbre, je me suis attaqué au problème de la surpêche à l'île Maurice.


Arbre des problèmes :


![](images/a_probleme.jpg)


Arbre des solutions :


![](images/a_solution.jpg)

## 5.2. Formation des groupes et formulation de la problématique

Pour la formation des groupes, nous nous sommes retrouvés dans une grande salle et nous devions amener un objet, j'ai personnellement ramené de l'herbe qui était une image pour les algues. Nous avons formé des groupes en fonction de l'objet qu'on avait amené qui pouvait être en lien avec l'objet des autres. On s'est retrouvé à 5 pour former notre groupe et trouver un problème commun.

Voici la feuille de match du XI de légende :

- [Suzanne](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/suzanne.lipski/)
- [Emma](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/emma.dubois/)
- [Alicia](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/Alicia.gimza/)
- [Chiara](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro/)

Notre problème commun était basé principalement sur la mer avec la destruction de sa biodiversité. Il existe différentes causes comme la pêche, les marées noires, le tourisme marin, les déchets en mer et les problèmes gravitant autour de ces sujets.

Les conséquences sont multiples, avec une perte majeure de la biodiversité, une barrière de corail en mauvaise santé, de moins en moins de poissons pour les pêcheurs locaux et une baisse du tourisme.

Cette baisse du tourisme peut entraîner de lourdes répercussions pour l'écosystème gravitant autour de ce pilier économique, comme les boutiques locales, les marchands, les artisans et tous les autres services reliés secondairement qui seront des dommages collatéraux de cette baisse.

En ayant pris connaissance des problèmes, nous avons premièrement listé nos idées de problématique :

![](images/idee.jpg)


### 5.2.1. Transformation de l'arbre des problèmes en arbre des objectifs

Notre but est d'apporter une solution à cette baisse de biodiversité. Pour cela, nous avons pensé à la mise en place d'une ferme marine en haute mer d'algue. Cette plantation marine va permettre de créer un écosystème autour de ces algues, cet écosystème engendrera une augmentation de la biodiversité qui redynamisera la barrière de corail.


De plus cela offrira de nouveaux emplois pour les pêcheurs pratiquant des pêches non responsable comme celle aux filets. Au-delà de tout ça, une branche du tourisme qui est celle de la plongée sera relancé grâce à ça.


![](images/sea_weed.jpg)


### Redirection

Après avoir beaucoup travaillé avec mon groupe, nous avons décidé de ne pas se lancer dans notre projet de base sur les algues, car nous n'avions pas trouvé de problème majeur pour la collecte des algues. Nous devions nous pencher sur ce point pour créer un prototype qui optimiserait cette étape.

Nous avons donc décidé de nous rediriger vers un prototype de kit utilisable en cas de marée noire qui est "DIY", c'est-à-dire qu'il est fabricable par sois même. Notre problème de base est donc les marées noires causées par les fuites de pétrole lors des naufrages sur les récifs coralliens en général.

Nous voulons créer ce kit qui sera composé de matériaux qui forment des boudins avec un rembourrage qui retient le pétrole à l'intérieur. Notre but est de le rendre accessible et facile à mettre en place en cas de marée noire. Nous voulons aussi créer une banque de données qui référence les différents types de matériaux de rembourrage accessible par rapport à la zone géographique qui peuvent être utilisé comme éponge dans les boudins.

Cette idée a par la suite encore évolué pour se concentrer sur l'optimisation de l'absorption du pétrole.

Voici quelque images ci-dessous de la [marée noire a l'île Maurice](https://www.lemonde.fr/afrique/article/2021/12/27/maree-noire-a-maurice-le-capitaine-du-bateau-et-son-second-condamnes-a-vingt-mois-de-prison_6107407_3212.html) en 2020 qui nous a inspiré.


![](images/wakashio1.jpg)



![](images/wakashio2.jpg)


## 5.3 Outils utilisés pour la dynamique de groupe


Nous avons abordé lors de ce cours de nombreuse technique pour prendre des décisions en groupe afin de trouver des solutions qui conviennent au maximum à tout le monde tout en restant rapide et efficace.De plus, nous avons aussi travaillé sur la répartition des taches et l'importance de se réunir pour prendre des décisions, dans certain cas, il n'est pas nécessaire de se réunir en personne si nous n'utilisons pas, par exemple les outils du fab lab.


Malgré tout, la prise de décision est importante, car ne prendre aucune décision est une prise de décision. On s'est principalement attardé sur les techniques les plus pertinentes pour notre groupe.


### 5.3.1 Planification des réunions en avance


Le fait de s'organiser avant la réunion en donnant des rôles spécifiques à chacun permet de se lancer plus rapidement dans le vif du sujet, de placer les objectifs et problèmes à résoudre et surtout de garder la réunion fluide en évitant les rencontres bureaucratiques.


Voici les techniques vu pendant le cours pour la répartition des tâches et la prise de décision :


![](images/tableau.jpg)


Les techniques qui nous ont le plus parler sont :


* **Météo** : Chacun donne son état d'esprit au moment présent pour éviter une mauvaise interprétation de la réaction d'un membre du groupe, par exemple un quelqu'un qui se plaint beaucoup n'est pas forcément en désaccord, mais manque peut être de sommeil.


* **ODJ** : "Ordre du jour" c'est la liste des objectifs et des tâches pour la réunion


* La répartition des tâches


* Faire un **planning** pour rester dans les temps





