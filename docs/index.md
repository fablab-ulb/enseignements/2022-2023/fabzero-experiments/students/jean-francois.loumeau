## Intro

Salut !

Je m'appelle Martin Loumeau, j'ai 21 ans, étudiant en biologie à l'ULB en bac 3.
Vous allez retrouver sur cette page une brève description de mes passions et un suivi de mes projets.

## Biographie
Je suis passionné par la musique (disco, african disco,acid,italo,etc) et l'océan. Je suis arrivé en Belgique en 2020 pour les études, je suis originaire de l'île Maurice.  Je passais auparavant la plupart de mon temps dans l'eau, pour plonger et surfer ou sur un terrain de rugby.
Je joue maintenant très peu au rugby à cause du froid, je me suis redirigé vers la boxe depuis peu. L'année prochaine, j'aimerais me diriger vers un master en biologie moléculaire à la VUB ou à l'université de Gent.

## Parcours
Mes études m'ont apporté beaucoup de théories et très peu de pratique, c'est exactement pour ça que je me suis inscrit au cours Fab zéro. Ce qui m'intéressait aussi beaucoup, c'était le mindset diriger vers la création de startup en démarrant de zéro et apprenant sur le tas.


![](images/smallimage.jpg)







